from pyglet.gl import *

pyglet.resource.path = ['./resources']
pyglet.resource.reindex()

def center_image(image):
    """Sets an image's anchor point to its center"""
    image.anchor_x = image.width // 2
    image.anchor_y = image.height // 2

starship_image = pyglet.resource.image("starship.png")
enemy_image = pyglet.resource.image("enemy.png")
firebolt_image = pyglet.resource.image("firebolt.png")
engine_image = pyglet.resource.image("firebolt.png")

center_image(starship_image)
center_image(enemy_image)
firebolt_image.anchor_x = firebolt_image.width * 0.9
firebolt_image.anchor_y = firebolt_image.height // 2
engine_image.anchor_x = engine_image.width  + starship_image.width // 2
engine_image.anchor_y = engine_image.height // 2