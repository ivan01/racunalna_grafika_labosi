from pyglet.gl import *
from pyglet.window import key
import math
from resources import resources
import random
_WINDOW_X = 1500
_WINDOW_Y = 800

_score = 0
_END = 0
game_objects =[]
enemies = []
walls = []
_START = True
_PAUSE = False
_enemy_increment = 0


game_window = pyglet.window.Window(_WINDOW_X, _WINDOW_Y)
class BulletObject(pyglet.sprite.Sprite):
    def __init__(self, x, y, rotation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scale = 5/self.height
        self.x = x
        self.y = y
        self.velocity = 600
        self.rotation = rotation
        angle_radians = -math.radians(self.rotation)
        self.velocity_x = math.cos(angle_radians) * self.velocity + random.random()*40
        self.velocity_y = math.sin(angle_radians) * self.velocity + random.random()*40
        self.dead = False
        pyglet.clock.schedule_once(self.die, 3)

    def die(self, dt):
        self.dead = True

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt

        global enemies
        to_remove = []
        for enemy in enemies:
            distance = math.sqrt(
                        (self.x - enemy.x) ** 2 +
                        (self.y - enemy.y) ** 2)
            if distance <= self.height/2 + enemy.width/2:
                enemy.die(dt)
                to_remove.append(enemy)
                global _score
                _score += 1
        for enemy in to_remove:
            enemies.remove(enemy)
    
class PlayerObject(pyglet.sprite.Sprite):

    def __init__(self, engine_sprite, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scale = 40/self.height
        self.velocity_x = 0
        self.velocity_y = 0
        self.thrust = 600.0
        self.rotate_speed = 200.0
        self.drag = 1
        self.keys = dict(left=False, right=False, up=False)
        self.key_handler = key.KeyStateHandler()
        self.engine_sprite = engine_sprite
        self.engine_sprite.scale = 40/self.engine_sprite.width
        self.engine_sprite.visible = False
        self.bullets = []
        self.dead = False
        self.bullet_count = 4

    def delete(self):
        self.engine_sprite.delete()
        super(PlayerObject, self).delete()

    def check_bounds(self):
        min_x = 0
        min_y = 0
        max_x = _WINDOW_X
        max_y = _WINDOW_Y
        if self.x < min_x:
            self.x = min_x
            self.velocity_x = -self.velocity_x
        elif self.x > max_x:
            self.x = max_x
            self.velocity_x = -self.velocity_x
        if self.y < min_y:
            self.y = min_y
            self.velocity_y = -self.velocity_y
        elif self.y > max_y:
            self.y = max_y
            self.velocity_y = -self.velocity_y

    def fire(self):
        if self.bullet_count <= 0:
            self.bullet_count = 4
            angle_radians = -math.radians(self.rotation)
            bullet_x = self.x + math.cos(angle_radians) * (self.width + 20)/2 
            bullet_y = self.y + math.sin(angle_radians) *  (self.width + 20)/2 
            self.bullets.append(BulletObject(bullet_x,bullet_y, self.rotation, img = resources.firebolt_image, batch = self.batch))
        else:
            self.bullet_count -=1
        

    
    def update(self, dt):
        if self.velocity_x > 1000:
            self.velocity_x = 1000
        if self.velocity_y > 1000:
            self.velocity_y = 1000
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt
        self.check_bounds()

        if self.key_handler[key.LEFT]:
            self.rotation -= self.rotate_speed * dt
        if self.key_handler[key.RIGHT]:
            self.rotation += self.rotate_speed * dt
        
        if self.key_handler[key.UP]:
            angle_radians = -math.radians(self.rotation)
            force_x = math.cos(angle_radians) * self.thrust 
            force_y = math.sin(angle_radians) * self.thrust 
            self.velocity_x += (force_x - self.velocity_x * self.drag) * dt 
            self.velocity_y += (force_y - self.velocity_y *self.drag) * dt
            self.engine_sprite.rotation = self.rotation
            self.engine_sprite.x = self.x
            self.engine_sprite.y = self.y
            self.engine_sprite.visible = True
        else:
            self.velocity_x -= self.velocity_x *self.drag*dt
            self.velocity_y -= self.velocity_y *self.drag*dt
            self.engine_sprite.visible = False
        
        if self.key_handler[key.SPACE]:
            self.fire()

class EnemyObject(pyglet.sprite.Sprite):

    def __init__(self, player, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.velocity_x = 0
        self.velocity_y = 0
        self.thrust = 400
        self.drag = 1
        self.player = player
        self.scale = 40/self.width
        self.dead = False
    
    def die(self, dt):
        self.dead = True

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt

        x_distance = self.player.x - self.x
        y_distance = self.y - self.player.y 
        if (abs(x_distance) < self.width/4 + self.player.width/2 and abs(y_distance) < self.width/4 + self.player.width/2):
            global _END 
            _END = 1

        angle_radians = - math.atan2(y_distance, x_distance)
        self.rotation = math.degrees(-angle_radians)

        force_x = math.cos(angle_radians) * self.thrust 
        force_y = math.sin(angle_radians) * self.thrust 
        self.velocity_x += (force_x - self.velocity_x * self.drag) * dt 
        self.velocity_y += (force_y - self.velocity_y * self.drag) * dt
    
class Wall(pyglet.shapes.Rectangle):
    def __init__(self, game_objects, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.anchor_x = self.width // 2
        self.anchor_y = self.height // 2
        self.game_objects = game_objects
    
    def transform_point(self, x, y, xo, yo, angle):
        temp_x = x - xo
        temp_y = y - yo
        new_x = math.cos(angle) * (temp_x) - math.sin(angle) * (temp_y)
        new_y = math.sin(angle) * (temp_x) + math.cos(angle) * (temp_y)
        return new_x, new_y
    
    def transform_back(self, x, y, xo, yo, angle):
        angle = -1*angle
        temp_x = math.cos(angle) * (x) - math.sin(angle) * (y)
        temp_y = math.sin(angle) * (x) + math.cos(angle) * (y)
        old_x = temp_x  + xo
        old_y = temp_y + yo
        return old_x, old_y
    

    def update(self, dt):
        for obj in self.game_objects:
            x, y = self.transform_point(obj.x, obj.y, self.x, self.y, math.radians(self.rotation))
            distance_x = abs(x) - obj.height/2
            distance_y = abs(y) - obj.height/2
            if (distance_x <= self.width/2 and distance_y < self.height/2):
                breach_x = self.width/2 - distance_x
                breach_y = self.height/2 - distance_y
                vel_x, vel_y = self.transform_point(obj.velocity_x, obj.velocity_y, 0, 0, math.radians(self.rotation))
                if (breach_x < breach_y):
                    x += math.copysign(breach_x, x)
                    vel_x = vel_x * -1
                else:
                    y += math.copysign(breach_y, y)
                    vel_y = vel_y * -1
                vel_x, vel_y = self.transform_back(vel_x, vel_y, 0, 0, math.radians(self.rotation))
                obj.velocity_x = vel_x
                obj.velocity_y = vel_y
                if (type(obj) == BulletObject):
                    obj.rotation = math.degrees(-math.atan2(vel_y, vel_x))
                x, y = self.transform_back(x, y, self.x, self.y, math.radians(self.rotation))
                obj.x = x
                obj.y = y
        
text_batch = pyglet.graphics.Batch()
end_label = pyglet.text.Label()
score_label = pyglet.text.Label(text=("Score: %d"%(_score)), x=10, y=_WINDOW_Y - 20, batch =text_batch)
background = pyglet.shapes.Rectangle(game_window.width//2-400, 0, 800, game_window.height, color= (0,0,0), batch = text_batch)
background.opacity = 125
level_label = pyglet.text.Label(text="Welcome to STARSHIP FLYER...press ENTER to start",
                            x=game_window.width//2, y=game_window.height//2, anchor_x='center', batch = text_batch)

main_batch = pyglet.graphics.Batch()
engine_sprite = pyglet.sprite.Sprite(img=resources.firebolt_image, batch = main_batch)
player_ship = PlayerObject(img= resources.starship_image,engine_sprite=engine_sprite, x=_WINDOW_X/2, y= _WINDOW_Y/2, batch = main_batch)
game_objects.append(player_ship)
game_window.push_handlers(player_ship.key_handler)

def create_enemies(dt, n):
    global _enemy_increment
    for _ in range(n + _enemy_increment):
        r = random.random()
        if (r < 0.25):
            enemy_x = random.randint(-50,0)
            enemy_y = random.randint(0, _WINDOW_Y)
        elif(r < 0.5):
            enemy_x = random.randint(_WINDOW_X,_WINDOW_X + 50)
            enemy_y = random.randint(0, _WINDOW_Y)
        elif (r < 0.75):
            enemy_x = random.randint(0,_WINDOW_X)
            enemy_y = random.randint(-50, 0)
        else:
            enemy_x = random.randint(0,_WINDOW_X)
            enemy_y = random.randint(_WINDOW_Y, _WINDOW_Y + 50)
        enemy = EnemyObject(img = resources.enemy_image, player = player_ship, x = enemy_x, y = enemy_y, batch = main_batch)
        game_objects.append(enemy)
        enemies.append(enemy)

def distance(point_1=(0, 0), point_2=(0, 0)):
    return math.sqrt((point_1[0] - point_2[0]) ** 2 + (point_1[1] - point_2[1]) ** 2)

walls_b = pyglet.graphics.Batch()
for _ in range(20):
    ship = player_ship.x, player_ship.y
    x, y = ship
    while distance((x,y), ship) < 250:
        x = random.random() * _WINDOW_X
        y = random.random() * _WINDOW_Y
    w = Wall(game_objects, x, y , random.random() * 200 + 50 , random.random() * 10 + 5, batch=walls_b)
    w.rotation = random.random() * 360
    walls.append(w)

def increase_enemy_increment(dt):
    global _enemy_increment
    _enemy_increment += 1


def on_key_press(symbol, modifiers):
    if symbol == key.RETURN:
        global _START
        global _END
        if _START:
            _START = False
            _END = False
            create_enemies(0, 10)
            pyglet.clock.schedule_interval(increase_enemy_increment, 3 )
            pyglet.clock.schedule_interval(create_enemies,3, n=random.randint(0,2))
            level_label.delete()
            background.visible = False
    if symbol == key.P:
        global _PAUSE
        _PAUSE = not _PAUSE
        

game_window.push_handlers(on_key_press)
def end():
    global _score
    end_label = pyglet.text.Label(text="GAME OVER",
                                x=game_window.width//2, y=game_window.height - 100, anchor_x='center', batch = text_batch, bold = True)
    end_label.font_size = 40
    score_label.x =game_window.width//2
    score_label.y =game_window.height - 150
    score_label.anchor_x='center'
    score_label.font_size = 20
    background.visible = True

    scores_label = pyglet.text.Label(text="HIGH SCORES:",
                                x=game_window.width//2, y=game_window.height - 200, anchor_x='center', batch = text_batch, bold = True)
    scores_label.font_size = 25

    y_pos = 250

    scores = []
    scores.append(_score)
    try:
        with open("scores.txt","r") as f:
            lines = f.readlines()
            for line in lines:
                scores.append(int(line))
            f.close()
    except IOError:
        pass

    scores.sort(reverse = True)
    for score in scores[:5]:
        label = pyglet.text.Label(text=str(score),
                                x=game_window.width//2, y=game_window.height - y_pos, anchor_x='center', batch = text_batch, bold = False)
        label.font_size = 15
        y_pos += 20
    f = open("scores.txt","w")
    for score in scores:
        f.write(str(score)+"\n")
    f.close()


def update(dt):
    score_label.text="Score: %d"%(_score)
    global _END, _START, _PAUSE
    if _START or _END == 2 or _PAUSE:
        dt = 0
    elif _END == 1:
        end()
        _END = 2
    else:
        game_objects.extend(game_objects[0].bullets)
        game_objects[0].bullets = [] 
        to_remove = []
        for obj in game_objects:
            if obj.dead:
                obj.delete()
                to_remove.append(obj)
            else:
                obj.update(dt)
        for obj in to_remove:
            game_objects.remove(obj)
        for wall in walls:
            wall.update(dt)

@game_window.event
def on_draw():
    game_window.clear()
    main_batch.draw()
    walls_b.draw()
    background.draw()
    text_batch.draw()

if __name__ == '__main__':
    pyglet.clock.schedule_interval(update, 1/120.0)
    pyglet.app.run()