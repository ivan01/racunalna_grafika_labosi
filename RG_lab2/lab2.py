from pyglet.gl import *
from pyglet.window import key
from pyrr import Vector3
from random import *
import numpy as np

class Camera:
    def __init__(self,pos=(0,0,0), lookAt=(0,400,0)):
        self.pos = list(pos)
        self.lookAt= list(lookAt)
    
    def get_postion(self):
        return self.pos
    
    def get_look_at(self):
        return self.lookAt

class Window(pyglet.window.Window):
    global pyglet

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_minimum_size(600, 400)
        self.POV = 60
        self.source = [0, 0, 0]
        self.camera = Camera((0,0,-1000))
        pyglet.clock.schedule_interval(self.update, 1.0 / 60.0)

        texture = pyglet.image.load('./smoke.bmp').get_texture()

        self.ParticleSys = ParticleSystem(texture, 5)

    def update(self, dt):
        self.ParticleSys.update(dt)

    def on_draw(self):
        self.clear()
        glClear(GL_COLOR_BUFFER_BIT)
        camera_position = self.camera.get_postion()
        lookAt = self.camera.get_look_at()
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(self.POV, self.width / self.height, 0.05, 10000)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        gluLookAt(camera_position[0], camera_position[1], camera_position[2],
                  lookAt[0], lookAt[1], lookAt[2],
                  0.0, 1.0, 0.0)
        glPushMatrix()
        self.ParticleSys.draw()
        glPopMatrix()
        glFlush()

    def on_key_press(self, key, modifiers):
        if key == pyglet.window.key.UP:
            self.camera.lookAt[1] += 50
        elif key == pyglet.window.key.DOWN:
            self.camera.lookAt[1] -= 50
        elif key == pyglet.window.key.RIGHT:
            self.camera.lookAt[0] += 50
        elif key == pyglet.window.key.LEFT:
            self.camera.lookAt[0] -= 50
        elif key == pyglet.window.key.W:
            self.ParticleSys.move(Vector3([0,0,30]))
        elif key == pyglet.window.key.S:
            self.ParticleSys.move(Vector3([0,0,-30]))
        elif key == pyglet.window.key.A:
            self.ParticleSys.move(Vector3([30,0,0]))
        elif key == pyglet.window.key.D:
            self.ParticleSys.move(Vector3([-30,0,0]))

class Particle:
    def __init__(self, pos, mass = 0.4, drag = 0.9, uplift = 15):
        self.pos = pos.copy()
        self.mass = mass
        self.drag = drag
        self.uplift = uplift
        self.vel = Vector3([randrange(-15,15), randrange(0, 15), randrange(-15,15)])
        self.lifeTime = 1.5

    def update(self, dt):
        force = -self.drag * self.vel
        force[1] += self.uplift
        self.vel = (force/self.mass)*dt + self.vel
        self.pos += self.vel
        self.lifeTime -= dt

class ParticleSystem:
    def __init__(self, texture, num=1, size=100, location=None):
        self.particles = []
        self.texture = texture
        self.size = size
        if location == None:
            self.location = [0,0,0]
        else:
            self.location = location
        self.addParticles(num)
        self.add_interval = 1.5
    
    def move(self, vector):
        self.location += vector

    def addParticles(self, num):
        for i in range(0, num):
            p = Particle(Vector3(self.location))
            self.particles.append(p)

    def update(self, dt):
        for p in self.particles:
            p.update(dt)
            if p.lifeTime <= 0:
                self.particles.remove(p)
        self.add_interval -= dt
        if (self.add_interval <= 0):
            self.addParticles(randint(30,40))
            self.add_interval = 1.5
        
        else: 
            self.addParticles(randint(0,1))

    def draw(self):
        glEnable(self.texture.target)
        glBindTexture(self.texture.target, self.texture.id)

        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)

        glPushMatrix()
        for p in self.particles:

            matrix = (GLfloat * 16)()
            glGetFloatv(GL_MODELVIEW_MATRIX, matrix)
            matrix = list(matrix)
            CameraUp = np.array([matrix[1], matrix[5], matrix[9]])
            CameraRight = np.array([matrix[0], matrix[4], matrix[8]])
            size = self.size

            v1 = p.pos + CameraRight * size + CameraUp * -size
            v2 = p.pos + CameraRight * size + CameraUp * size
            v3 = p.pos + CameraRight * -size + CameraUp * -size
            v4 = p.pos + CameraRight * -size + CameraUp * size

            glBegin(GL_QUADS)
            glTexCoord2f(0, 0)
            glVertex3f(v3[0], v3[1], v3[2])
            glTexCoord2f(1, 0)
            glVertex3f(v4[0], v4[1], v4[2])
            glTexCoord2f(1, 1)
            glVertex3f(v2[0], v2[1], v2[2])
            glTexCoord2f(0, 1)
            glVertex3f(v1[0], v1[1], v1[2])

            glEnd()
        glDisable(GL_BLEND)
        glPopMatrix()
        glDisable(self.texture.target)



def main():
    window = Window(width=1500, height=700,
                    caption='Particle system', resizable=True)
    keys = key.KeyStateHandler()
    window.push_handlers(keys)

    pyglet.app.run()

main()